﻿import React from 'react';
import ReactDOM from 'react-dom';
//import 'semantic-ui-css/semantic.min.css';
//import './node_modules/semantic-ui-css/semantic-ui-css';
import { Button, Header, Footer, Modal, Table } from 'semantic-ui-react';


//export class AddCus extends React.Component {
//    constructor() {
//        super();
//        this.state = {
//            modalOpen: false
//        };
//        this.handleOpen = this.handleOpen.bind(this);
//       this.handleClose = this.handleClose.bind(this);
//    }
//    handleOpen=() =>{
//        this.setState({ modalOpen: true });
//    }
//    handleClose = () => {
//        this.setState({ modalOpen: false });
//    }
//    render() {
//        return (
//            <div>
//                <Button onClick={this.handleOpen}>Show Modal</Button>
//                <Modal open={this.state.modalOpen} onClose={this.handleClose}>
//                    <Modal.Content>
//                        <h3>This website uses cookies to ensure the best user experience.</h3>
//                    </Modal.Content>
//                    <Modal.Actions>
//                        <Button onClick={this.handleClose}>Got it</Button>
//                    </Modal.Actions>
//                </Modal>
//            </div>
//        )
//    }
//}

class CustomerRow extends React.Component {
    render() {
        return (
            <tr>
                <td>{this.props.item.CustomerName}</td>
                <td>{this.props.item.Address}</td>
                <td><Button id="edtbtn" className="btn btn-warning">Edit</Button></td>
                <td><Button id = "dltbtn"className="btn btn-danger">Delete</Button></td>
            </tr>
            );
    }
}
export class CustomerTable extends React.Component {
    constructor() {
        super();

        this.state = {
            items: [],
        };

    }

    componentDidMount() {
        $.get(this.props.dataUrl, function (data) {
                this.setState({
                    items: data
                });  
        }.bind(this));
    }
    render() {

        var rows = [];
        this.state.items.forEach(function (item) {
            rows.push(<CustomerRow key={item.CustomerId} item={item} />);
        });
        return (
            <table className="table table-striped table-bordered table-responsive table-hover">
                <thead>
                    <tr>
                    <th> Customer Name</th>
                    <th>Address</th>
                    <th>Edit</th>
                    <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    {rows}
                    </tbody>
                </table>);
    }
}
class AddCustomer extends React.Component {
    constructor() {
        super();
        this.state = {open: false}

        this.createcustomer = this.createcustomer.bind(this);
    }
    createcustomer() {
        setState = {open: true}
       // alert();
    }

    render() {
        return (
            <div>
                <button className="btn btn-primary" onClick={this.createcustomer}> Add Customer</button> 
           
                <Modal show={this.state.open}>
                    <Modal.Header>
                        <Modal.Title>Modal heading</Modal.Title>
                    </Modal.Header>
                </Modal>
            </div>
        );
    }


}


export class Customer extends React.Component {
    render() {
        return (
            <div>
                <AddCustomer />
                <br></br>
                <CustomerTable dataUrl="/Customer/GetCustomers" />
            </div>
            );
    }
}

ReactDOM.render(
    <Customer/>,
    document.getElementById('customer')
)