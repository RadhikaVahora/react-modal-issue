﻿using React_OnBoarding.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace React_OnBoarding.Controllers
{
    public class CustomerController : Controller
    {
        private DbModel db = new DbModel();
        // GET: Customer
        public ActionResult Index()
        {
            return View();
        }

        public JsonResult GetCustomers() {

            var data = db.Customer.ToList();
            return new JsonResult {Data = data, JsonRequestBehavior =JsonRequestBehavior.AllowGet };
        }

        
    }
}